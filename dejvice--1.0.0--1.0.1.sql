alter table @extschema@.domacnosti add primary key (id);

alter table @extschema@.clenove add foreign key (c_domacnosti) references @extschema@.domacnosti (id);

create index ON @extschema@.clenove (c_domacnosti);
